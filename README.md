# ClubMaker Biblioteca Ignasi Iglesias - Introducció a Docker

Aquest repositori és una petita introducció i guia per instal·lar [Pi-Hole](https://pi-hole.net/) i [Whoogle](https://github.com/benbusby/whoogle-search) com a excusa per aprendre Docker. Ens basarem en una imatge acabada d'instal·lar de Raspbian Stretch.

**Aquest projecte té només finalitats educatives i no s'explica com exposar aquests serveis a Internet**.

## Configuració bàsica de la màquina

### Descarregar la darrera imatge de Raspbian i gravar-la a una SD

Depenent del sistema operatiu aquest procediment es pot fer d'una manera o una altra, però en general la forma més senzilla és fer servir [Etcher](https://www.balena.io/etcher/) de Balena.

Un cop gravada la imatge extreiem la SD i la tornem a posar a l'ordinador. Veurem dues noves unitat de disc: boot i rootfs. A la unitat boot afegirem dos arxius: `ssh` i `wpa_supplicant.conf`. El primer és una arxiu buit (sense contingut) que farà que en arrencar la Raspberry activi el serfvidor SSH per poder accedir-hi remotament, el segon ens permet configurar una WiFi (si no volem connectar-la via cable amb DHCP). Els continguts d'asquest segon arxiu seran (editant el SSID i PASS):

```
country=ES
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1

network={
    ssid="<ssid>"
    psk="<pass>"
}
```

Extreiem la SD amb seguretat i la posem a la Raspberry i l'arrenquem. Esperem uns 90 segons a que arrenqui i provem de connectar-nos via SSH (des de la línía de comandes a Linux o amb Putty a Windows):

```
ssh pi@raspberrypi.local
```

Acceptem el certificat i introduïm el password per defecte que és `raspberry`.

### Primeres passes

**NOTEU**: En les instruccions a continuació és símbol del dolar ($) és el `prompt` que indica que el sistema espera una comanda, el símbol mateix **no s'ha de copiar**, només el que ve a continuació. Es recomana copiar les instruccions d'una en una, línia a línia.


* Comprovem la connectivitat

```
$ ping google.com
```

* Actualitzam els repositori i el programari carregat:

```
$ sudo apt-get update
$ sudo apt-get upgrade
```

* Configurem les opcions pròpies de la Raspberry (activar SSH, expandir el sistema de fitxers, canviar el nom de la màquina, editar les opcions de localització,...)

```
$ sudo raspi-config
```

* Opcionalment re-creem la clau del host (això farà saltar un avís el proper cop que entrem a la màquina):

```
$ sudo rm /etc/ssh/ssh_host* 
$ sudo ssh-keygen -A
```

* Creem un usuari nou (recomanat)...

```
$ sudo adduser xose
$ sudo adduser xose sudo
```

Ara sortim i tornem a accedir amb el nou usuari, comprovem que té permís sudo fent:

```
$ sudo whoami
[sudo] contrasenya per a ...:
root
```

* I eliminem l'usuari pi amb el seu directori *home*:

```
$ sudo deluser --remove-home pi
```

...o canviem el password de l'usuari `pi`:

```
$ sudo passwd pi
```

* Reiniciem:

```
$ sudo reboot
```

* Si hem canviat el nom de la màquina ara haurem d'accedir amb el nou nom:

```
$ ssh xose@rpi3.local
```

## Instal·lació dels serveis

### Instal·lar docker

Instal·lar `docker` no pot ser més fàcil:

```
$ curl -sSL https://get.docker.com/ | sh
```

Es recomana afegir l'usuari actual al grup docker per poder gestionar images i contenidors sense ser root (hauràs de sortir i tornar a entrar a la Raspberry Pi per que aquest canvi tingui efecte):

```
$ sudo adduser $USER docker
```

Comprova que està instal·lat i que tens els permisos:

```
$ docker --version
Docker version 19.03.9, build 9d98839
$ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
```

### Instal·lar docker-compose

Instal·la `docker-compose`:

```
$ sudo apt-get install -y libffi-dev libssl-dev python3 python3-pip
$ sudo pip3 install docker-compose
```

Comprova que està instal·lat:

```
$ docker-compose --version
docker-compose version 1.25.5, build unknown
```

### Clonar el repositori

Clone el repositori a la raspberry pi:

```
$ sudo apt install git
$ git clone https://gitlab.com/xoseperez/clubmaker-rpi3-docker.git
$ cd clubmaker-rpi3-docker
```

Per últim editarem les variables de l'arxiu `enviroment`. Aquest arxiu conté variables de configuració per fer-les servir des de tots els contenidors que aixequien. Haurem de canviar algunes, especialment els passwords i paths i després copiar-lo a `/etc/`:

```
$ nano environment
$ sudo cp environment /etc/
```

I sortim i tornem a entrar a la raspberry per que es carreguin els valors de les variables per defecte.

## Serveis

### Blynk

Servidor Blynk per fer-lo servir des de la app Blynk de Android o iOS, permet crear dashboards de visualització i acció de dades.

### PiHole

PiHole és un servidor DNS local amb l'afegit de que pots afegir llistats de dominis a bloquejar (malware, pàgines de jocs, pàgines d'adults,...). Un cop instal·lat o bé defineixes a les DNS del teu router la IP de la màquina on tens corrent PiHole (i aleshores totes les màquines de la xarxa passaran per aquest filtre) o bé ho fas ordinador per ordinador al formulari de configuració de la connexió.

### Portainer

Portainer és un gestor de contenidors docker via web, es pot consultar l'estat d'execució dels contenidors, aturar-los, re-engegar-los,...

### Whoogle

Whoogle és un proxy per el buscador de google que neteja les cerques eliminant anuncis i codi de tracking, i es pot integrar fàcilment en el teu navegador favorit.
## Primeres passes amb Docker

Conceptes clau a entendre:

* Imatge (image): un arxiu composat de múltiples capes amb tot el necessari per generar un contenidor. És inmutable.
* Contenidor (container): una instància d'una imatge en execució o pausada
* Registre (registry): un lloc web des d'on Docker pot descarrear automàticament imatges
* Amfitrió (host): màquina "física" o s'executen els contenidors
* Xarxa (network): una xarxa interna per un o vàrios contenidors, pot incloure o no el host
* Volum (volume): directori del host que es munta dins el contenidor, permet persistir les dades del contenidor
* Màquina (engine): procés principal encarregat de la gestió dels contenidors
* Eixam (swarm):  conjunt d'amfitrions executant la màquina de docker i compartint serveis
* Servei (service): conjunt de contenidors distribuïts en diferents amfitrions executant el mateix contenidor de manera distribuida
* Secret (secret): sistema per passar dades de configuració encriptades a un contenidor

Docker Compose és un script que permet definir la configuració de contenidors, xarxes, ports,... a través d'arxius de configuració. No és necessari per executar contenidors però ajuda a portar la configuració d'una màquina a altra. Els directoris d'aquest repositori contenen arxius `docker-compose.yml` amb la configuració dels diferents serveis.

### Descarregar i executar una imatge

```
$ cd pihole
$ docker-compose pull
$ docker-compose up -d
```

Ara podem intentar accedir a la plana d'administració des del navegador a `http://rpi3.local/admin` i fer login amb el password que hem definit a l'arxiu `environment`.

Podem fer el mateix amb el directori `portainer` que aixecarà un gestor de contenidors amb entorn visual:

```
$ cd ../portainer
$ docker-compose pull
$ docker-compose up -d
```

I anem a `http://rpi3.local:9000`, la primera vegada ens demana que creem un compte d'administrador.

### Construir una imatge pròpia

El tercer contenidor del projecte, a diferència dels altres dos, no té una imatge associada, sino qe s'ha de fer un `build` (contruir la imatge) i aquest procés triga una mica més, tot i que només s'ha de fer un cop:

```
$ cd ..
$ cd whoogle
$ docker-compose build
$ docker-compose up -d
```

I podem accedir al buscador a `http://rpi3.local:5000`.

### Inspeccionar el log d'un contenidor

De vegades és necessari inspeccionar els missatges que un contenidor genera, la manera més fàcil és fent servir la comanda `log` de docker:

```
$ docker logs -f blynk
```

O des del mateix directori on tinguem el `docker-compose.yml` d'un servei:

```
$ docker-compose logs -f
```

## Licència

Copyright (C) 2020 by Xose Pérez (@xoseperez)
per a el ClubMaker de la Biblioteca Ignasi Iglesias

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
